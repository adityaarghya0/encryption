# encryption

A secure encryption program

IMPORTANT: Download the full repository as zip file. Extract the zip to a folder and open package.buej with bluej java ide only.

This project encrypts and decrypts text files into a non decipherable format. The default password "aditya5102" (without quotes) The encrypted files are saved in C:\EncryptedFiles. The files to be encrypted can be kept in C:\NonEncryptedFiles. The file name can then be typed into the program. The program runs the best with BlueJ java ide with jdk 8 or up Execute the class Main. Run the main method.

IMPORTANT: A text encrypted with n layers of encryption can be decrypted with n layers of encryption only. For example, text file, abc.txt is encrypted with 5 layers of encryprion and saved as abc.invisibleCipher Then, the file, abc.invisibleCipher can be decrypted only when the user sets the layers of encryption to 5.

When encrypting a text to a file, the file name you type at the top will be the file name of the encrypted file. For example, if you type "abcd" as the file name, the file, after encryption will be saved as "abcd.invisibleCipher" in C:/EncryptedFiles. While decrypting, you need to type "abcd" in file name (WITHOUT EXTENSION OF ".invisibleCipher).